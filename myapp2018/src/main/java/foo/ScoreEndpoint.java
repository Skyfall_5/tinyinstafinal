package foo;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PreparedQuery.TooManyResultsException;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import com.google.appengine.api.datastore.Key;


@Api(name = "myApi",
version = "v1",
namespace = @ApiNamespace(ownerDomain = "helloworld.example.com",
    ownerName = "helloworld.example.com",
    packagePath = ""))

public class ScoreEndpoint {
	
	//----------------------------------------------------------------------------------------
	
	
	 @ApiMethod(name = "addUser", httpMethod = HttpMethod.POST, path ="User")
     public Entity addUser(@Named("username") String username, @Named("name") String name, @Named("nickname") String nickname) {
             DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
             Query q = new Query("User").setFilter(new FilterPredicate(Entity.KEY_RESERVED_PROPERTY , FilterOperator.EQUAL,KeyFactory.createKey("User", username))); 
             PreparedQuery querry = datastore.prepare(q);
             querry = datastore.prepare(q);
             
             if (querry.countEntities(FetchOptions.Builder.withLimit(1))!=0) {
                     return new Entity("Reponse", "user is already in the datastore");
             }else {
                     Entity e = new Entity("User", username);
                     e.setProperty("username", username);
                     e.setProperty("name", name);
                     e.setProperty("nickname", nickname);
                     e.setProperty("followers", new ArrayList<Key>());
                     e.setProperty("followings", new ArrayList<Key>());
                     datastore.put(e);
                     return e;
             }
             
     
     }
     
     @ApiMethod(name = "addPost", httpMethod = HttpMethod.POST, path="User/{username}/Post")
     public Entity addPost(@Named("username") String username, @Named("message") String message, @Named("picture") String picture, @Named("likesCompteur") String likesCompteur ) {
             DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

             // rajouter le id post et user id
             Entity e = new Entity("Post", username);
             e.setProperty("username", username);
             e.setProperty("message", message);
             e.setProperty("picture", picture);
             e.setProperty("likesCompteur", likesCompteur);
             datastore.put(e);
             return e;
     }
     
     @ApiMethod(name = "removeUser", httpMethod = HttpMethod.DELETE, path="User/{username}")
     public Entity removeUser(@Named("username") String username) {
    	 	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService(); 
    	 	Query q = new Query("User").setFilter(new FilterPredicate(Entity.KEY_RESERVED_PROPERTY , FilterOperator.EQUAL,KeyFactory.createKey("User", username)));
             PreparedQuery querry = datastore.prepare(q);
             Entity result = querry.asSingleEntity();
             
             datastore.delete(result.getKey());
             
             return new Entity("Reponse", "user delete from datastore");
     }
     
     
     @ApiMethod(name = "follow", httpMethod = HttpMethod.PUT, path = "follow")
 public Entity follow(@Named("follower") String follower, @Named("followed") String followed) {
             DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
             
             //Verify if the follower exists
             Query q =new Query("User").setFilter(new FilterPredicate(Entity.KEY_RESERVED_PROPERTY ,FilterOperator.EQUAL, KeyFactory.createKey("User", follower)));
             PreparedQuery pq = datastore.prepare(q);

             if(pq.countEntities(FetchOptions.Builder.withLimit(1))!=1) {
            	 q =new Query("User").setFilter(new FilterPredicate(Entity.KEY_RESERVED_PROPERTY,FilterOperator.EQUAL, KeyFactory.createKey("User", followed)));
                 pq = datastore.prepare(q);
                 if (pq.countEntities(FetchOptions.Builder.withLimit(1))!=1) {
                	 return new Entity("Reponse", "unable to follow the user");
                 }
             }
             
             Entity user1 = getUser(follower);
             Entity user2 = getUser(followed);
             
             List<Key> followings = (List<Key>) user1.getProperty("followings");
             List<Key> followers = (List<Key>) user2.getProperty("followers");
            
             if (followings == null) {
            	 followings = new ArrayList<Key>();
             }
             if (followers == null) {
            	 followers = new ArrayList<Key>();
             }
             
             followings.add(user2.getKey());
             followers.add(user1.getKey());
             user1.setProperty("followings", followings);
             user2.setProperty("followers", followers);
             
             datastore.put(user1);
             datastore.put(user2);
     
             return new Entity("Reponse", "user1 follows user2");
 }
 
  @ApiMethod(name="getUser", httpMethod = HttpMethod.GET, path="username")   
 public Entity getUser(@Named("username") String username) {
	  DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	  Query q =new Query("User").setFilter(new FilterPredicate(Entity.KEY_RESERVED_PROPERTY ,FilterOperator.EQUAL, KeyFactory.createKey("User", username)));
	  PreparedQuery pq = datastore.prepare(q);
	  return pq.asSingleEntity();
 }
     
@ApiMethod(name="getFollowers", httpMethod = HttpMethod.GET, path="followers")  
public List<Entity> getFollowers(@Named("username") String username){
	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	Entity user = getUser(username);
	List<Key> followers = (List<Key>) user.getProperty("followers");
	if (followers == null) {
		new ArrayList<Entity>();
	}
	Map<Key, Entity> followersFromTheUser = datastore.get(followers);
	return new ArrayList<Entity>(followersFromTheUser.values());
	
}
     
@ApiMethod(name="getFollowings", httpMethod = HttpMethod.GET, path="followings")  
public List<Entity> getFollowings(@Named("username") String username){
	DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	Entity user = getUser(username);
	List<Key> followings = (List<Key>) user.getProperty("followings");
	if (followings == null) {
		new ArrayList<Entity>();
	}
	Map<Key, Entity> followingsFromTheUser = datastore.get(followings);
	return new ArrayList<Entity>(followingsFromTheUser.values());
	
}

	
}