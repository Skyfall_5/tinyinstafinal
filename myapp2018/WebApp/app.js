const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const path = require('path');
const net = require('net');

const app = express();

// Passport Config
require('./config/passport')(passport);
app.use(express.static(path.join(__dirname,"/public")));

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose.connect(db, { useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

// EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Express body parser
app.use(express.urlencoded({ extended: true }));

// Express session
app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Global variables
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});

// Routes
app.use('/', require('./routes/index.js'));
app.use('/users', require('./routes/users.js'));

const PORT = process.env.PORT || 8080;

app.listen(PORT, console.log(`Server started on port ${PORT}`));


// Communication NodeJS-JAVA via les sockets

var HOST = '0.0.0.0'; // parameterisation de l'IP pour l'ECOUTE
var PORT2 = '1204'

// Creer une instance du serveur et attendre la connexion
net.createServer(function(sock) {


  // Receives a connection - a socket object is associated to the connection automatically
  console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);


  // Add a 'data' - "event handler" in this socket instance
  sock.on('data', function(data) {
    // data was received in the socket 
    // Writes the received message back to the socket (echo)
    sock.write("NODEHERE: "+data);
  });


  // Add a 'close' - "event handler" in this socket instance
  sock.on('close', function(data) {
    // closed connection
    console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
  });


}).listen(PORT2, HOST);

// if(false){
//   var client = net.connect(1203, '0.0.0.0');
//   client.write('Hello from node.js');
//   client.end();
// }
// console.log('Server also listening on ' + HOST +':'+ PORT);


