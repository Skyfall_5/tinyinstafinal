# Projet tinyInstagram

Réalisé par Amine BOULAHMEL, Julien LOMBAERDE et Juliana MÉTIVIER

# Préambule 

Nous avons rencontré de nombreux problèmes afin de définir le projet mais aussi afin d'accèder aux services REST proposés par Google. 
( vous trouverez sur ce lien notre permière version du tinyInstagram nous provoquant de nombreuses erreurs sur le google datastore: https://gitlab.com/Skyfall_5/tinyinstagram )
Comme nous avons eu du mal à accèder à la commande _ah/api/explorer , nous avons opté pour une solution plus simple: celle de réutiliser le projet https://github.com/momo54/webandcloud
en redéfinissant le scoreEndpoint pour qu'il contienne un ensemble de fonctionnalités demandées. 
Nous préférons être sincères: nous n'avons pas toutes les fonctionnalités demandées. 


# Lancement de notre projet:

1. https://webcloud-1.appspot.com/_ah/api/explorer pour accèder au services REST de Google.
2. https://tinyinsta-259220.appspot.com  pour accèder à l'interface programmée pour le projet. Vous trouverez 3 captures d'écran sur le repository illustrant notre interface actuelle.  
